<?php


namespace Esol\LocationBundle\Service\City;


use Doctrine\ORM\EntityManagerInterface;
use Esol\LocationBundle\Entity\City;

class CityManager implements CityManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * CityManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addCity(array $parameters)
    {
        // TODO: Implement addCity() method.
    }

    public function getCities()
    {
        $ret = array('valid' =>false,'msg' =>null,'value' =>null);
        $cities = $this->entityManager->getRepository(City::class)->findAll();
        if($cities!=null){
            $ret['valid'] = true;
            $ret['value'] = $cities;
        }
        return $ret;
    }

    public function getCity(int $id)
    {
        // TODO: Implement getCity() method.
    }

    public function updateCity(array $parameters)
    {
        // TODO: Implement updateCity() method.
    }

    public function deleteCity(int $id)
    {
        // TODO: Implement deleteCity() method.
    }
}