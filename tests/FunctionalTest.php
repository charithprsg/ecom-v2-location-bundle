<?php


namespace Esol\LocationBundle\Tests;


use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Esol\LocationBundle\EsolLocationBundle;
use Esol\LocationBundle\Service\LocationManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;



class FunctionalTest extends TestCase
{
    public function testServiceWiring(){
        $kernel = new EsolLocationTestingKernel('test',true);
        $kernel->boot();
        $container = $kernel->getContainer();
        $locationManager =$container->get('esol.location_manager');
        $locationManager->getCities();


    }


}

class EsolLocationTestingKernel extends Kernel{

    public function registerBundles()
    {
        return [
            new EsolLocationBundle(),new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Symfony\ORM
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
    }
}