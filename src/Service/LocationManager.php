<?php


namespace Esol\LocationBundle\Service;


use Esol\LocationBundle\Service\City\CityManagerInterface;

class LocationManager implements LocationManagerInterface
{
    /**
     * @var CityManagerInterface
     */
    private $cityManager;


    /**
     * LocationManager constructor.
     */
    public function __construct(CityManagerInterface $cityManager)
    {
        $this->cityManager = $cityManager;
    }

    public function manageAddCity(array $parameters)
    {
        // TODO: Implement manageAddCity() method.
    }

    public function manageGetCities()
    {
        $cities = $this->cityManager->getCities();
        if($cities['valid']){
            return $cities['value'];
        }
        return null;
    }

    public function manageGetCity(int $id)
    {
        // TODO: Implement manageGetCity() method.
    }

    public function manageUpdateCity(array $parameters)
    {
        // TODO: Implement manageUpdateCity() method.
    }

    public function manageDeleteCity(int $id)
    {
        // TODO: Implement manageDeleteCity() method.
    }

}