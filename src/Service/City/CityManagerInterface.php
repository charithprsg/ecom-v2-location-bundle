<?php


namespace Esol\LocationBundle\Service\City;


interface CityManagerInterface
{
    public function addCity(array $parameters);
    public function getCities();
    public function getCity(int $id);
    public function updateCity(array $parameters);
    public function deleteCity(int $id);
}