<?php


namespace Esol\LocationBundle\Listener;
use Esol\AdminBundle\Event\AdminBundleSideBarConfigurationEvent;

class LocationBundleConfigurationListener
{
    private static $config = array('bundleName' => 'LocationBundle','navigations'=>array('list'=>'esol.location-list'));
    private $configuration;

    /**
     * LocationBundleConfigurationListener constructor.
     * @param $configuration
     */
    public function __construct()
    {
        $this->configuration = self::$config;
    }


    public function onConfigurationView(AdminBundleSideBarConfigurationEvent $event){
        $this->configuration = $event->addConfiguration(self::$config);
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
}