<?php


namespace Esol\LocationBundle\Controller;


use Esol\LocationBundle\Service\LocationManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends AbstractController
{
    /**
     * @var LocationManagerInterface
     */
    private $locationManager;

    /**
     * LocationController constructor.
     */
    public function __construct(LocationManagerInterface $locationManager)
    {
        $this->locationManager = $locationManager;
    }

    public function getCities(Request $request){
        $cities = $this->locationManager->manageGetCities();
        return $this->render('@EsolLocation/city/list.html.twig',array('cities'=>$cities));
    }

    public function addCity(Request $request){
        $cities = $this->locationManager->manageGetCities();
        return $this->render('@EsolLocation/city/create.html.twig',array('cities'=>$cities));
    }
}