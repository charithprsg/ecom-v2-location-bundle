<?php


namespace Esol\LocationBundle\Service;


interface LocationManagerInterface
{
    public function manageAddCity(array $parameters);
    public function manageGetCities();
    public function manageGetCity(int $id);
    public function manageUpdateCity(array $parameters);
    public function manageDeleteCity(int $id);
}